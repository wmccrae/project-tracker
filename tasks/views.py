from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Task
from .forms import TaskForm


# Allow the logged in user to create a new task for any project and
# assign it to any registered user.
@login_required
def create_task(request):
    if request.method == "POST":
        # We should use the form to validate the values
        #   and save them to the database
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            # If all goes well, we can redirect the browser
            #   to another page and leave the function
            return redirect("list_projects")
    else:
        # Create an instance of the Django model form class
        form = TaskForm()
    # Put the form in the context
    context = {
        "form": form,
    }
    # Render the HTML template with the form
    return render(request, "tasks/createtask.html", context)


# Allow the logged in user to see their tasks.
@login_required
def user_tasks(request):
    user_tasklist = Task.objects.filter(assignee=request.user)
    context = {
        "usertasklist": user_tasklist,
    }
    return render(request, "tasks/tasklist.html", context)
