Project Tracker

Wanda M. - Fullstack design

Once logged in, a user can create, list, edit, and delete projects and task lists for each project.

Back end: Python with Django
Front End: HTML5 with CSS
